<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();



Route::middleware(['auth'])->group(function () {
    Route::get('greet/create',[App\Http\Controllers\GreatController::class,'create'])->name('great.create');
    Route::post('greet',[App\Http\Controllers\GreatController::class,'store'])->name('great.store');
});


Route::get('greet/{id}',[App\Http\Controllers\GreatController::class,'view'])->name('great.view');

