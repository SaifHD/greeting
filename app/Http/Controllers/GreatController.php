<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Greet;
use App\Http\Requests\GreatRequest;
use App\Mail\GreetMail;
use Illuminate\Support\Facades\Mail;

class GreatController extends Controller
{
    //
    public function create(){
        return view('greet.create_greet');
    }
    public function store(GreatRequest $request){
        $greet=new Greet();
        $greet->user_id=auth()->user()->id;
        $greet->title=$request->input('title');
        $greet->email=$request->input('email');
        $greet->message=$request->input('message');
        $greet->save();

        $greet_id=$greet->id;
        $message=$request->message;
        $title=$request->title;
        $user_email=auth()->user()->email;
        $username=auth()->user()->name;
        Mail::to($greet->email)->send(new GreetMail($username,$message,$title,$greet_id,$user_email));


            return redirect()->back();


    }
    public function view($id){
        $greet=Greet::findOrFail($id);
        return view('greet.preview_greet',compact('greet'));
    }
}
