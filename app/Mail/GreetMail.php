<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GreetMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $message;
    public $title;
    public $username;
    public $reply;
    public $id;

    public function __construct($username,$message,$title,$id,$reply)
    {
        //
        $this->message=$message;
        $this->title=$title;
        $this->username=$username;
        $this->id=$id;
        $this->reply=$reply;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.greet_mail')->
        with(['title'=>$this->title,'message'=>$this->message,'id'=>$this->id])->subject("New Greet")
        ->replyTo($this->reply,$this->username);
    }
}
