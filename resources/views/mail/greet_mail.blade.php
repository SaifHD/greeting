
@component('mail::message')
{{ $title }}
<br>

{{ $message }}

@component('mail::button', ['url' => env("APP_URL").'/'.'greet/'.$id])
View Greet
@endcomponent

Thanks,<br>
{{ $username }}
@endcomponent
