@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>Send Greating</h2></div>
                <div class="container">
                    <br>
                    <form action="{{ route('great.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label >Title</label>
                            <input type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder="Enter The Title">
                        </div>
                        <div class="form-group">
                            <label>Recipient’s Email Address</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="name@example.com">
                        </div>



                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control" name="message" rows="3">{{ old('message') }}</textarea>
                        </div>
                        <br>
                        <div class="form-group">
                            <button class="btn btn-success btn-lg form-control">Send Greeting</button>
                        </div>

                    </form>
                    <br>


                </div>

            </div>
        </div>
    </div>
</div>
@endsection
